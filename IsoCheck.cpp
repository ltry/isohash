#include "IsoCheck.h"
#include "Generator.h"

#include <time.h>
#include <random>
#include <algorithm>
namespace Glib{
    extern std::mt19937 _gen;

    bool IsoPre(Graph const& g1, Graph const& g2){
        if (g1.n != g2.n) return 0;
        if (g1.edges.size() != g2.edges.size()) return 0;

        std::vector <int> v1(g1.n), v2(g2.n);
        for (auto i: g1.edges)
            ++ v1[i.first-1], ++ v1[i.second-1];
        for (auto i: g2.edges)
            ++ v2[i.first-1], ++ v2[i.second-1];

        std::sort(v1.begin(), v1.end());
        std::sort(v2.begin(), v2.end());
        if (v1 != v2) return 0;

        return 1;
    }

    static bool CheckIdxs(Graph const& g1, Graph const& g2, std::vector <int> const& rnd, std::vector <int> const& new_idxs){
        std::vector <int> sH(g2.n);
        for (auto i: g2.edges){
            sH[i.first-1] += rnd[i.second-1];
            sH[i.second-1] += rnd[i.first-1];
        }

        std::vector <int> fH(g1.n);
        for (auto i: g1.edges){
            int u = new_idxs[i.first-1],
                v = new_idxs[i.second-1];
            fH[u] += rnd[v];
            fH[v] += rnd[u];
        }
        return fH == sH;
    }

    bool IsoPerm(Graph const& g1, Graph const& g2, std::vector <int>* new_idxs){
        if (!IsoPre(g1, g2)) return 0;
        int n = g1.n;
        std::vector <int> idxs(n), rnd(n);
        iota(idxs.begin(), idxs.end(), 0);

        for (int i = 0; i < n; ++ i)
            rnd[i] = _gen()%int(1e7)+1e7;

        char flag = 0;
        do flag = CheckIdxs(g1, g2, rnd, idxs);
        while (!flag && std::next_permutation(idxs.begin(), idxs.end()));

        if (flag && new_idxs != nullptr)
            *new_idxs = idxs;
        return flag;
    }
    bool IsoRnd(Graph const& g1, Graph const& g2, std::vector <int>* new_idxs, int times){
        if (!IsoPre(g1, g2)) return 0;
        int n = g1.n;
        std::vector <int> idxs(n), rnd(n);
        iota(idxs.begin(), idxs.end(), 0);

        for (int i = 0; i < n; ++ i)
            rnd[i] = _gen()%int(1e7)+1e7;

        char flag = 0;
        for (int T = 0; T < times && !flag; ++ T){
            std::shuffle(idxs.begin(), idxs.end(), _gen);
            flag = CheckIdxs(g1, g2, rnd, idxs);
        }
        if (flag && new_idxs != nullptr)
            *new_idxs = idxs;
        return flag;
    }
}
