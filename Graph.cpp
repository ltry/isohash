#include "Graph.h"
#include "AdjList.h"

namespace Glib{
    void read(Graph& trg){
        int m;
        std::cin >> trg.n >> m;
        for (int i = 0; i < m; ++ i){
            int u, v;
            std::cin >> u >> v;

            trg.edges.push_back({u, v});
        }
    }

    void write(Graph const& src){
        std::cout << src.n << " " << src.edges.size() << "\n";
        for (auto i: src.edges)
            std::cout << i.first << " " << i.second << "\n";
    }

    Graph::operator AdjList() const{
        AdjList res;
        res.g.resize(n);
        for (auto i: edges){
            res.g[i.first-1].push_back(i.second-1);
            res.g[i.second-1].push_back(i.first-1);
        }
        return res;
    }
}
