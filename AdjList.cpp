#include "Graph.h"
#include "AdjList.h"

namespace Glib{
    AdjList::operator Graph() const{
        Graph res(g.size());
        for (int u = 0; u < g.size(); ++ u)
        for (int v: g[u]) if (u < v)
            res.edges.emplace_back(u+1, v+1);
        return res;
    }
}
