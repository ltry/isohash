#include "IsoHash.h"
#include "Generator.h"

#include <random>
namespace Glib{
    extern std::mt19937 _gen;

    const ll GB = 10000, GE = 200000;
    std::uniform_int_distribution <ll> dist(GB, GE);
    inline ll get_next(ll cur, std::map <ll, ll>* next){
        if (!next->count(cur))
            (*next)[cur] = dist(_gen);
        return (*next)[cur];
    }
    ll IsoHash(Graph const& g, std::map <ll, ll>* next){
        std:: map <ll, ll> _next;
        if (next == nullptr) next = &_next;

        int n = g.n;
        const int L = n;

        std::vector <ll> H[2];
        H[0] = H[1] = std::vector <ll> (n);

        int o = 0;
        for (auto e: g.edges){
            H[o][e.first-1] += 1;
            H[o][e.second-1] += 1;
        }

        for (int lev = 0; lev < L; ++ lev){
            o ^= 1;
            std::fill(H[o].begin(), H[o].end(), 0);

            for (auto e: g.edges){
                H[o][e.first-1] += H[o^1][e.second-1];
                H[o][e.second-1] += H[o^1][e.first-1];
            }

            for (int i = 0; i < n; ++ i)
                H[o][i] = get_next(H[o][i], next);
        }

        ll res = 0;
        for (int i = 0; i < n; ++ i)
            res += H[o][i];
        return res;
    }
}
