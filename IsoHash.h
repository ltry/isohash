#include "Graph.h"

#include <map>
namespace Glib{
    typedef long long ll;
    ll IsoHash(Graph const& g, std::map <ll, ll>* next);
}
