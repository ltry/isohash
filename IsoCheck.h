#ifndef ISOCHECK_H
#define ISOCHECK_H

#include "Graph.h"
namespace Glib{
    bool IsoPre(Graph const& g1, Graph const& g2);
    bool IsoPerm(Graph const& g1, Graph const& g2, std::vector <int>* new_idxs);
    bool IsoRnd(Graph const& g1, Graph const& g2, std::vector <int>* new_idxs, int times);
    static bool CheckIdxs(Graph const& g1, Graph const& g2,
        std::vector <int> const& rnd, std::vector <int> const& new_idxs);
}

#endif // ISOCHECK_H
