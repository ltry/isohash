#ifndef ADJLIST_H
#define ADJLIST_H

#include <vector>
namespace Glib{
    struct Graph;

    struct AdjList{
        std::vector <std::vector <int> > g;
        operator Graph() const;
    };
}

#endif // ADJLIST_H
