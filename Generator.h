#ifndef GENERATOR_H
#define GENERATOR_H


#include "Graph.h"

namespace Glib{
    void genArbitrary(Graph& trg, int n, int m);
    void genConnected(Graph& trg, int n, int m);
    void genTree(Graph& trg, int n);
    void genForest(Graph& trg, int n, int m);
    void shuffleVertices(Graph& trg);
}

#endif //GENERATOR_H
