#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include <iostream>
namespace Glib{
    struct AdjList;

    typedef std::vector <std::pair <int, int> > E;
    struct Graph{
        int n;
        E edges;

        Graph(int n = 0, E const& edges = E()): n(n), edges(edges) {}
        Graph(Graph const& other): n(other.n), edges(other.edges) {}
        Graph(Graph&& other): n(other.n), edges(std::move(edges)) {}

        Graph& operator=(Graph const& other){
            n = other.n, edges = other.edges;
            return *this;
        }
        Graph& operator=(Graph&& other){
            n = other.n, edges = std::move(other.edges);
            return *this;
        }

        operator AdjList() const;
    };

    void read(Graph& trg);
    void write(Graph const& src);
}

#endif // GRAPH_H
