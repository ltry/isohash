#include "Generator.h"

#include <time.h>
#include <random>
#include <algorithm>
#include <functional>
#include <unordered_set>

namespace Glib{
    typedef long double ld;
    typedef long long ll;
    const ld dense = 0.4;

    std::mt19937 _gen(3711+time(NULL));
    void genArbitrary(Graph& trg, int n, int m){
        trg.n = n, trg.edges.clear();
        if (!m) return;

        ld D = ld(2*m)/(ld(n)*ld(n-1));
        if (D > 1.0) return;

        if (D > dense){
            E all;
            for (int i = 1; i <= n; ++ i)
            for (int j = i+1; j <= n; ++ j)
                all.emplace_back(i, j);
            for (int i = 0; i < m; ++ i)
                swap(all[i], all[i+_gen()%(m-i)]);
            for (int i = 0; i < m; ++ i)
                trg.edges.push_back(all[i]);
            return;
        }

        std::unordered_set <int> was;
        was.reserve(4*m);
        was.max_load_factor(0.25);

        while (was.size() < m){
            int u = _gen()%n,
                v = _gen()%(n-1);
            if (v >= u) ++ v;
            if (u > v) std::swap(u, v);
            if (was.count(u*n+v))
                continue;
            was.insert(u*n+v);
            trg.edges.emplace_back(u+1, v+1);
        }
    }
    void genTree(Graph& trg, int n){
        trg.n = n, trg.edges.clear();

        for (int i = 1; i < n; ++ i)
            trg.edges.emplace_back(1+i, 1+_gen()%i);
        shuffleVertices(trg);
    }
    void genConnected(Graph& trg, int n, int m){
        trg.n = n, trg.edges.clear();
        if (m < n-1) return;

        genTree(trg, n);
        if (m == trg.edges.size()) return;

        std::unordered_set <int> was;
        was.reserve(4*m);
        was.max_load_factor(0.25);

        for (auto e: trg.edges){
            int u = std::min(e.first, e.second),
                v = std::max(e.first, e.second);
            was.insert((u-1)*n+(v-1));
        }

        while (was.size() < m){
            int u = _gen()%n,
                v = _gen()%(n-1);
            if (v >= u) ++ v;
            if (u > v) std::swap(u, v);
            if (was.count(u*n+v))
                continue;
            was.insert(u*n+v);
            trg.edges.emplace_back(u+1, v+1);
        }
    }
    void genForest(Graph& trg, int n, int m){
        trg.n = n, trg.edges.clear();
        if ((n*1LL*(n-1))/2LL < ll(m))
            return;

        std::vector <int> p(n), o(n);
        iota(p.begin(), p.end(), 0);

        std::function <int(int)> root =
            [&p, &root](int u){ return p[u] == u ? u : p[u] = root(p[u]); };

        std::function <void(int, int)> unite =
            [&p, &o, &root](int u, int v){
                u = root(u), v = root(v);
                if (o[u] > o[v]) p[v] = u;
                else if (o[u] < o[v]) p[u] = v;
                    else ++ o[u], p[v] = u;
            };

        for (int i = 0; i < m; ++ i){
            int u = _gen()%n,
                v = _gen()%(n-1);
            if (v >= u) ++ v;

            if (root(u) == root(v)) continue;
            else { unite(u, v), trg.edges.emplace_back(u+1, v+1); }
        }
    }
    void shuffleVertices(Graph& trg){
        std::vector <int> nw(trg.n);
        iota(nw.begin(), nw.end(), 0);
        shuffle(nw.begin(), nw.end(), _gen);

        for (auto& i: trg.edges){
            i.first = nw[i.first-1]+1;
            i.second = nw[i.second-1]+1;
        }
    }
}
